class MessagesController < ApplicationController
  before_action :authenticate_user!, except: :create

  before_action :set_quest

  def create
    @message = Message.new(message_params)
    @message.quest = @quest
    @message.user = current_user if params[:user] == "true"

    if @message.save
      redirect_to @quest, notice: 'Message was successfully created.'
    else
      render "quests/show"
    end
  end

  def index
    @message = Message.new
    render "quests/show"
  end

  def payout
    return permission_denied unless current_user == @quest.user
    message = @quest.messages.find(params[:message_id])
    respond_to do |format|
      if @quest.payout(message)
        format.html { redirect_to @quest, notice: 'Payout was a success.' }
        format.json { render :show, status: :created, location: @quest }
      else
        format.html { redirect_to @quest, notice: 'Something went wrong!' }
        format.json { render json: @quest.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_quest
    @quest = Quest.find(params[:quest_id])
  end

  def message_params
    params.require(:message).permit(:text, :address)
  end

end
