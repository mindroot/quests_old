class SolutionPayoutJob < ApplicationJob
  queue_as :default
  require 'iota'

  def perform(quest, solution)
      client = IOTA::Client.new(provider: 'https://iota.nodes24.com')

      account = IOTA::Models::Account.new(client, quest.seed)
      account.getAccountDetails({})
      balance  = account.balance.to_i

      transfer = IOTA::Models::Transfer.new(
          address: solution.address,
          value: balance,
          tag: 'M9QUESTS'
      )

      depth = 3
      minWeightMagnitude = 14

      # If you want library to fetch input and make transfer
      # TODO: Check response
      response = account.sendTransfer(depth, minWeightMagnitude, [transfer], {})
      pp "response"
      pp response

      quest.payed_at = Time.zone.now
      quest.payed_amount = balance
      quest.inactive!
      quest.save!
  end
end
