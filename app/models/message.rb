class Message < ApplicationRecord
  belongs_to :quest
  belongs_to :user, optional: true

  validates_length_of :text, :minimum => 2, :maximum => 3000
  validate :valid_iota_address

  require 'iota'

  def valid_iota_address
    utils = IOTA::Utils::InputValidator.new
    errors.add(:address, "Not a valid iota address.") unless utils.isAddress(address)
    pp errors.to_json
  end

end
