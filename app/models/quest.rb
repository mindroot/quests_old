class Quest < ApplicationRecord

  enum status: [:active, :payout_proccessed, :inactive]

  before_create :create_address
  require 'iota'

  has_many :messages
  belongs_to :user

  extend FriendlyId
  friendly_id :title, use: [:slugged, :history, :finders]
  def should_generate_new_friendly_id?
    title_changed?
  end

  validates :title, :slug, presence: true
  validates :title, length: { minimum: 5, maximum: 81 }
  validates :description, length: { minimum: 5, maximum: 3000 }

  def create_address
    # Create client directly with provider
    client = IOTA::Client.new(provider: 'https://iota.nodes24.com')

    # now you can start using all of the functions
    status, data = client.api.getNodeInfo

    ## create and save seed
    self.seed = (1..81).map{'ABCDEFGHIJKLMNOPQRSTUVWXYZ9'[SecureRandom.random_number(27)]}.join('')

    ## create and save latest address
    account = IOTA::Models::Account.new(client, self.seed)
    account.getAccountDetails({})

    self.latest_address = account.latestAddress
    self.status = :active

  end

  def solver
    solver_id == 0 ? 'anonymous' : User.find(solver_id).name
  end

  def solution
    solution_id == 0 ? nil : messages.find(solution_id)
  end

  def payout message

    utils = IOTA::Utils::InputValidator.new
    return false unless utils.isAddress(message.address)


    self.solver_id = message.user.id if message.user
    self.solution_id = message.id
    SolutionPayoutJob.perform_later(self, message)
    self.payout_proccessed!

   return true
  end
end
