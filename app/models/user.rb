class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
        :lockable, :confirmable, :trackable

  has_many :quests

  extend FriendlyId
  friendly_id :name, use: [:slugged, :history, :finders]
  def should_generate_new_friendly_id?
    name_changed?
  end

  validates :name, :slug, presence: true
  validates :name, uniqueness: true, length: { minimum: 1, maximum: 100 }

end
