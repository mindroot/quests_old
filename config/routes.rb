Rails.application.routes.draw do
  get 'how_to', to: 'home#how_to'
  get 'privacy', to: 'home#privacy'
  devise_for :users
  resources :quests do
     resources :messages do
       post 'payout'
     end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'quests#index'
end
