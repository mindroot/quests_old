class CreateQuests < ActiveRecord::Migration[5.2]
  def change
    create_table :quests do |t|
      t.string :title
      t.string :description

      t.string :seed
      t.string :latest_address
      
      t.timestamps
    end
  end
end
