class AddStatusToQuest < ActiveRecord::Migration[5.2]
  def change
    add_column :quests, :status, :integer
  end
end
