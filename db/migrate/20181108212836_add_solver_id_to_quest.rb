class AddSolverIdToQuest < ActiveRecord::Migration[5.2]
  def change
    add_column :quests, :solver_id, :integer, default: 0
  end
end
