class AddSolutionToQuest < ActiveRecord::Migration[5.2]
  def change
    add_column :quests, :solution_id, :integer, default: 0
  end
end
