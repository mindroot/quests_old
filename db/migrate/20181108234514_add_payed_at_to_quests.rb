class AddPayedAtToQuests < ActiveRecord::Migration[5.2]
  def change
    add_column :quests, :payed_at, :datetime
  end
end
