class AddSlugToQuest < ActiveRecord::Migration[5.2]
  def change
    add_column :quests, :slug, :string
  end
end
