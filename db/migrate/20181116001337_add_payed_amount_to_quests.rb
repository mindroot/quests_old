class AddPayedAmountToQuests < ActiveRecord::Migration[5.2]
  def change
    add_column :quests, :payed_amount, :integer
  end
end
