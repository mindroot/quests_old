# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

USER = [
  {
    email: 'user1@user.de',
    name: 'user1',
    password: 'useruser'
  },
  {
    email: 'user2@user.de',
    name: 'user2',
    password: 'useruser'
  }
]

def create_users
  USER.each do |user_params|
    user = User.create!(user_params)
    user.confirm
    pp "user - #{user.name} created."
  end
end

def create_quests
  User.all.each do |user|
    for index in 1..5
      quest = Quest.create!(title: "#{index} Quest of #{user.name}", description: "Lorem ipsum.", user: user)
      pp "quest '#{quest.title}' created."
    end
  end
end

create_users
create_quests
